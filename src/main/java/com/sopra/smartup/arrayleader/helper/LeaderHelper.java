package com.sopra.smartup.arrayleader.helper;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

public class LeaderHelper {
	
	/**
	 * Given an input array returns all the number occurrences in array.
	 * 
	 * @param inputArray representing the input.
	 * @return a map with the numbers and their occurrences.
	 */
	public Map<Integer, Long> getOccurrencesNumber(int[] inputArray) {
		return Arrays.stream(inputArray)
				     .boxed()
 				     .collect(groupingBy(Function.identity(), counting()));
		
	}
	
	/**
	 * Given the map with numbers and their occurrences and the size of input array
	 * extract the leaders of array.
	 * 
	 * @param numbersMap is a map with the numbers and their occurrences in array.
	 * @param sizeList is the size of input array.
	 * @return a list with the leaders of array.
	 */
	public List<Integer> extractLeaders(Map<Integer, Long> numbersMap, Integer sizeList) {
		return numbersMap.entrySet()
						 .parallelStream()
						 .filter(e -> e.getValue() > (sizeList / 2))
						 .map(Map.Entry::getKey)
						 .sorted()
						 .collect(toList());
	}

}
