package com.sopra.smartup.arrayleader.helper;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

public class LeaderHelperTest {

	@Test
	public void should_ReturnMapWithOcurrences_When_GetOccurrencesNumberReceivedData() {
		// Given
		int[] inputArray = {2, 3, 2, 4, 3};
		Map<Integer, Long> expectedMap = new HashMap<>();
		expectedMap.put(2, 2L);
		expectedMap.put(3, 2L);
		expectedMap.put(4, 1L);
		// When
		LeaderHelper helper = new LeaderHelper();
		Map<Integer, Long> mapResult = helper.getOccurrencesNumber(inputArray);
		// Then
		assertEquals(expectedMap, mapResult);
 	}
	
	@Test
	public void should_ReturnEmptyMap_When_GetOccurrencesNumberEmptyArray() {
		// Given
		int[] inputArray = {};
		Map<Integer, Long> expectedMap = new HashMap<>();
		// When
		LeaderHelper helper = new LeaderHelper();
		Map<Integer, Long> mapResult = helper.getOccurrencesNumber(inputArray);
		// Then
		assertEquals(expectedMap, mapResult);
	}
	
	@Test
	public void should_ReturnLeaders_When_ExtractLeadersReceivedMapAndSize() {
		// Given
		Map<Integer, Long> inputMap = new HashMap<>();
		inputMap.put(2, 3L);
		inputMap.put(3, 1L);
		inputMap.put(4, 1L);
		Integer sizeInputArray = 5;
		// When
		LeaderHelper helper = new LeaderHelper();
		List<Integer> leaders = helper.extractLeaders(inputMap, sizeInputArray);
		// Then
		assertEquals(1, leaders.size());
		assertEquals(Integer.valueOf(2), leaders.get(0));
	}
	
	@Test
	public void should_ReturnEmptyList_When_ExtractLeadersReceivedEmptyMap() {
		// Given
		Map<Integer, Long> inputMap = new HashMap<>();
		Integer sizeInputArray = 5;
		// When
		LeaderHelper helper = new LeaderHelper();
		List<Integer> leaders = helper.extractLeaders(inputMap, sizeInputArray);
		// Then
		assertEquals(0, leaders.size());
	}
	
}
