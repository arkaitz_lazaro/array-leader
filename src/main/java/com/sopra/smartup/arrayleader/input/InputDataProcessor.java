package com.sopra.smartup.arrayleader.input;

import java.util.Scanner;

import com.sopra.smartup.arrayleader.exception.InvalidParameterException;
import com.sopra.smartup.arrayleader.input.model.InputData;

public class InputDataProcessor {
	
	public static InputData loadLeaderInputFromStdin() {
		Scanner reader = new Scanner(System.in);
		System.out.println("Enter the input array size:");
		int arraySize = reader.nextInt();
		int[] inputArray = new int[arraySize];
		for (int i = 0; i < arraySize; i++) {
		  System.out.println("Enter an element to array: \n");
		  int num = reader.nextInt();
		  inputArray[i] = num;
		}
		System.out.println("Enter 'k' parameter: ");
		int kParameter = reader.nextInt();
		System.out.println("Enter 'm' parameter: ");
		int mParameter = reader.nextInt();
		reader.close();
		return InputData.builder()
					    .kParameter(kParameter)
				        .mParameter(mParameter)
				        .data(inputArray)
				        .build();
	}

	public static InputData loadLeaderInputFromArguments(String[] arguments) throws InvalidParameterException {
		if (arguments.length == 3) {
			String[] arrayDataStr = arguments[0].split(",");
			
			int[] arrayInput = new int[arrayDataStr.length];
		    for (int i = 0; i < arrayDataStr.length; i++) {
		      arrayInput[i] = Integer.parseInt(arrayDataStr[i]);
		    }
		    int kParameter = Integer.parseInt(arguments[1]);
		    int mParameter = Integer.parseInt(arguments[2]);
			return InputData.builder()
					        .kParameter(kParameter)
					        .mParameter(mParameter)
					        .data(arrayInput)
					        .build();
		} else {
			throw new InvalidParameterException("ERROR: Must introduce 3 parameters: Array, k and m parameters");
		}
	}
}
