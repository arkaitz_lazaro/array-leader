package com.sopra.smartup.arrayleader.exception;

public class InvalidParameterException extends Exception {

	private static final long serialVersionUID = 7668425143497774646L;
	
	public InvalidParameterException(String message) {
		super(message);
	}

}
