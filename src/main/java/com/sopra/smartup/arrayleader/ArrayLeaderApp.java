package com.sopra.smartup.arrayleader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.sopra.smartup.arrayleader.exception.InvalidParameterException;
import com.sopra.smartup.arrayleader.helper.LeaderHelper;
import com.sopra.smartup.arrayleader.input.InputDataProcessor;
import com.sopra.smartup.arrayleader.input.model.InputData;

public class ArrayLeaderApp {
	
	private static final Logger logger = Logger.getLogger(ArrayLeaderApp.class.getName());
    
	public static void main(String[] args) {
    	logger.info("Starting Array Leader app ...");
		ArrayLeaderApp app = new ArrayLeaderApp();
		    	
		try {
			InputData inputData = (args.length > 0) ? 
					InputDataProcessor.loadLeaderInputFromArguments(args) : 
					InputDataProcessor.loadLeaderInputFromStdin();
			logger.log(Level.INFO, "Array data: {0}", Arrays.toString(inputData.getData()));
			logger.log(Level.INFO, "K parameter: {0}", inputData.getKParameter());
			logger.log(Level.INFO, "M parameter: {0}", inputData.getMParameter());
			List<Integer> solution = app.computeSolution(inputData);
			logger.log(Level.INFO, "Leaders retrieved: {0}", solution);
		} catch (InvalidParameterException e) {
			System.out.println(e.getMessage());
		} catch (Exception e) {
			System.out.println("ERROR: An error occurred during program execution");
		}
    }

	/**
	 * Function that compute the solution to the array leader problem
	 * 
	 * @param k indicates the segment length of array 'A'
	 * @param m is the maximum value of each element in the array
	 * @param a represents the input array
	 * @return an array with all the numbers that can become a leader 
	 */
	public List<Integer> computeSolution(InputData inputData) {
	    
		int[] input = inputData.getData().clone();
		int k = inputData.getKParameter();
				
		LeaderHelper helper = new LeaderHelper();
		Map<Integer, Long> map;
		Set<Integer> leaders = new HashSet<>();
		int i=0;
		while (i <= input.length - k) {
		  for (int j=i; j<i+k; j++) {
			input[j] = input[j] + 1;
		  }
		  map = helper.getOccurrencesNumber(input);
		  leaders.addAll(helper.extractLeaders(map, input.length));
		  input = inputData.getData().clone();
		  i++;
		}
		return new ArrayList<>(leaders);
	}
	
}
