package com.sopra.smartup.arrayleader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import com.sopra.smartup.arrayleader.input.model.InputData;

import org.junit.Test;

public class ArrayLeaderAppTest {
	
	@Test
	public void should_ReturnSolution_When_computeSolutionFound() {
		// Given
		int[] inputArray = {2, 2, 2, 3, 4};
		int kParameter = 3;
		int mParameter = 4;
		InputData input = new InputData(kParameter, mParameter, inputArray);
		// When
		ArrayLeaderApp app =  new ArrayLeaderApp();
		List<Integer> leaders = app.computeSolution(input);
		// Then
		assertEquals(1, leaders.size());
		assertEquals(Integer.valueOf(3), leaders.get(0));
		
	}
	
	@Test
	public void should_ReturnEmptyList_When_computeSolutionNotFound() {
		// Given
		int[] inputArray = {1, 2, 4, 3, 5};
		int kParameter = 3;
		int mParameter = 4;
		InputData input = new InputData(kParameter, mParameter, inputArray);
		// When
		ArrayLeaderApp app =  new ArrayLeaderApp();
		List<Integer> leaders = app.computeSolution(input);
		// Then
		assertTrue(leaders.isEmpty());
	}
}
