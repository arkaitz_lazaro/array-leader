package com.sopra.smartup.arrayleader.input;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.sopra.smartup.arrayleader.exception.InvalidParameterException;
import com.sopra.smartup.arrayleader.input.model.InputData;

public class InputDataProcessorTest {

	@Test
	public void should_ReturnInputDataWithData_When_loadLeaderInputFromArgumentsCorrect() throws InvalidParameterException {
	  // Given
	  String[] dataArguments = new String[3];
	  dataArguments[0] = "2,3,4,5";
	  dataArguments[1] = "2";
	  dataArguments[2] = "5";
	  // When
	  InputData dataReturned = InputDataProcessor.loadLeaderInputFromArguments(dataArguments);
	  // Then
	  assertArrayEquals(new int[] {2,3,4,5}, dataReturned.getData());
	  assertEquals(4, dataReturned.getData().length);
	  assertEquals(Integer.valueOf("2"), dataReturned.getKParameter());
	  assertEquals(Integer.valueOf("5"), dataReturned.getMParameter());
	}
	
	@Test(expected = InvalidParameterException.class)
	public void should_ThrowException_When_loadLeaderInputFromArgumentsIncorrect() throws InvalidParameterException {
	  // Given
	  String[] dataArgs = new String[2];
	  dataArgs[0] = "2,3,4,5";
	  dataArgs[1] = "2";
	  // When
	  InputDataProcessor.loadLeaderInputFromArguments(dataArgs);
	  // Then, throws InvalidParameterException
	}
}
