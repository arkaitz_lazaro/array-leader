package com.sopra.smartup.arrayleader.input.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class InputData {

	private Integer kParameter;
	
	private Integer mParameter;
	
	private int[] data;
	
	public InputData() {}

	public InputData(Integer kParameter, Integer mParameter, int[] data) {
		super();
		this.kParameter = kParameter;
		this.mParameter = mParameter;
		this.data = data;
	}
	
}
